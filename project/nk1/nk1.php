<?php
/**
 * Plugin Name:	NK One
 * Version: 		0.1.0
 * Author:  		Navdeep kaur
 * Description: 	This plugin is for project for CPSC 2030.
 * Author URI:  	https://deepkaursardarni2.gitlab.io
 * Plugin URI:  	https://navdeepkaursardarni.chickenkiller.com/Project2030
 */
add_filter('posts_orderby', 'nk1_alphabetical' );


function nk1_alphabetical( $orderby )
{
  global $nk1_category;
  
  $nk1_category="Uncategorized";

  if( is_category( $nk1_category )) {
     // alphabetical order by post title
     return "post_title ASC";
  }

  // not in nk1 category, return default order by
  return $orderby;
}




//
//
// custom taxonomy
//
//

//hook into the init action and call create_topics_nonhierarchical_taxonomy when it fires
 
add_action( 'init', 'create_topics_nonhierarchical_taxonomy', 0 );
 
function create_topics_nonhierarchical_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Topics', 'taxonomy general name' ),
    'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Topics' ),
    'popular_items' => __( 'Popular Topics' ),
    'all_items' => __( 'All Topics' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Topic' ), 
    'update_item' => __( 'Update Topic' ),
    'add_new_item' => __( 'Add New Topic' ),
    'new_item_name' => __( 'New Topic Name' ),
    'separate_items_with_commas' => __( 'Separate topics with commas' ),
    'add_or_remove_items' => __( 'Add or remove topics' ),
    'choose_from_most_used' => __( 'Choose from the most used topics' ),
    'menu_name' => __( 'Topics' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('topics','post',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}


?>