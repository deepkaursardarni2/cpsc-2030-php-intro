<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
	$parent_style = 'royal-style'; // use actual parent style
 
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'projectTheme',      	// use actual child style
    		get_stylesheet_directory_uri() . '/style.css',
    		array( $parent_style ),
    		wp_get_theme()->get('Version')
	);
}

//require_once "/aria-walker-nav-menu.php";


function cptui_register_my_cpts_recipe() {

	/**
	 * Post Type: recipes.
	 */

	$labels = array(
		"name" => __( "recipes", "projectTheme" ),
		"singular_name" => __( "recipe", "projectTheme" ),
	);

	$args = array(
		"label" => __( "recipes", "projectTheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "recipe", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "custom-fields", "comments", "post-formats" ),
		"taxonomies" => array( "topics" ),
	);

	register_post_type( "recipe", $args );
}

add_action( 'init', 'cptui_register_my_cpts_recipe' );



?>
