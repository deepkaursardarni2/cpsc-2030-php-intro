<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Royal
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php if( get_theme_mod('apple_touch') ) : ?>
	<!-- For first- and second-generation iPad: -->
	<link rel="apple-touch-icon" href="<?php echo esc_url( get_theme_mod( 'apple_touch' ) ); ?>">
<?php endif; ?>

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>    
<div id="page" class="hfeed site <?php echo royal_site_style_class(); ?>">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'royal' ); ?></a>
	<?php do_action('royal_before_header'); ?>
	<header id="masthead" class="site-header  <?php echo royal_site_style_header_class(); ?>" role="banner">
		<div class="header-wrap branding  header-image">
		<?php if ( get_theme_mod ('header_overlay',false ) ) { 
				   echo '<div class="overlay overlay-header"></div>';     
				} ?>
			<div class="container">	
				<div class="four columns">
					<div class="contact">  
						<?php dynamic_sidebar('top-left' ); ?>
					</div>
				</div>
			<div class="branding eight columns">
				<div class="site-branding">
				<?php 
					$logo_title = get_theme_mod( 'logo_title' );
							$logo = get_theme_mod( 'logo', '' );
			

							$tagline = get_theme_mod( 'tagline',true);
							if( $logo_title && function_exists( 'the_custom_logo' ) ) {
                                the_custom_logo();     
					        }elseif( $logo != '' && $logo_title ) { ?>
							   <h1 class="site-title img-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url($logo) ?>"></a></h1>
					<?php	}else { ?>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						    <?php } ?>

				 <?php if( $tagline ) : ?>
						<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
					<?php endif; ?>
				</div>
			</div>			
			<?php if( is_active_sidebar( 'top-right' ) ) : ?>
				<div class="four columns">
					<div class="social">
						<?php dynamic_sidebar('top-right' ); ?>
					</div>
				</div>
			<?php endif; ?>		
		</div>
	</div>
		<div class="nav-wrap" id="nav-wrap">
			<div class="container">
				<?php if ( get_theme_mod ('header_search',true) ){  ?>
					<nav id="site-navigation" class="main-navigation clearfix thirteen columns" role="navigation">
						<button class="menu-toggle" aria-controls="menu" aria-expanded="false"><?php echo apply_filters('royal_responsive_menu_title',__('Primary Menu','royal') ); ?></button>
						<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
					</nav><!-- #site-navigation -->
					<div class="three columns">
						<div id="search">
							<?php get_search_form(); ?>
							<span class="search-box"><i class="fa fa-search"></i></span>
						</div>
					</div>
				<?php } else { ?>
					<nav id="site-navigation" class="main-navigation clearfix sixteen columns" role="navigation">
						<button class="menu-toggle" aria-controls="menu" aria-expanded="false"><?php _e( 'Primary Menu', 'royal' ); ?></button>
						<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
					</nav><!-- #site-navigation -->
	
				<?php } ?>
				<?php do_action('royal_after_primary_nav'); ?>
			</div>
		</div>		 	
	</header><!-- #masthead -->
 <?php do_action('royal_after_header'); ?>  
<?php if ( function_exists( 'is_woocommerce' ) || function_exists( 'is_cart' ) || function_exists( 'is_chechout' ) ) :
 if ( is_woocommerce() || is_cart() || is_checkout() ) { ?>    
	<div class="breadcrumb-wrap">
	<div class="container">
		<div class="sixteen columns breadcrumb">	
			<header class="entry-header">
				<h1 class="entry-title"><?php woocommerce_page_title(); ?></h1>
			</header><!-- .entry-header -->
			<?php $breadcrumb = get_theme_mod( 'breadcrumb',true ); 
				if( $breadcrumb ) : ?>
				<div id="breadcrumb" role="navigation">
					<?php woocommerce_breadcrumb(); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php } 
endif; ?>

<nav class="collapse  navbar-collapse" role="navigation" aria-label="<?php _e( 'Main Menu', 'textdomain' ); ?>">
  <?php
    if ( has_nav_menu( 'main-menu' ) ) {
      wp_nav_menu( array(
        'theme_location' => 'main-menu',
        'container'      => false,
        'menu_class'     => 'main-navigation',
        'walker'         => new Aria_Walker_Nav_Menu(),
        'items_wrap'     => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>',
      ) );
    }
  ?>
</nav>