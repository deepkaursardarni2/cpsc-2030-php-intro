<?php 

 session_start();
   
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2030</title>
  </head>
  <body class="container">
    <h1>Cars - CPSC 2030</h1>
    
     
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Make</th>
                <th scope="col">Model</th>
                <th scope="col">Mileage</th>
                <th scope="col">Year</th>
            </tr>
        </thead>
        <tbody>
           
            <?php
             $link = mysqli_connect( 'localhost', 'root', '' );
            mysqli_select_db( $link, 'cars2' );
            ?>
            
            
            
            <?php if($_SESSION["loggedin"]==false){?>
                    <button class="btn btn-primary btn-lg "><a style="color:white"href=login.php>login</a></button>
            <?php }?>
            
           
            <?php if( $_SESSION["loggedin"]==true) { ?>
                  <h4>you are now logged in </h4>
                   <button class="btn btn-primary btn-lg "><a style="color:white"href=logout.php>logout</a></button>
                    
            
                <form class="needs-validation" novalidate method="POST" action="">
                    <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="make">Make</label>
                        <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
                        <div class="invalid-feedback">
                          Valid make is required.
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="model">Model</label>
                        <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
                        <div class="invalid-feedback">
                          Valid make is required.
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="mileage">mileage</label>
                        <input type="text" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
                        <div class="invalid-feedback">
                          Valid make is required.
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="year">Year</label>
                        <input type="text" class="form-control" id="year" placeholder="" value="" required name="year">
                        <div class="invalid-feedback">
                          Valid make is required.
                        </div>
                      </div>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Add</button>
                    
                          
                      </div>
                     
                      
                    </div>
   
                  </form>
                <tbody>
                   
            <?php
             $link = mysqli_connect( 'localhost', 'root', 'smiley00' );
            mysqli_select_db( $link, 'cars2' );
           
            // process $results
          
           

            
           //&&($_REQUEST["OPERATION"])=="ADD"
             $OPERATION=mysqli_real_escape_string($link,$_REQUEST["OPERATION"]);
             if ( isset( $_REQUEST["make"] )) {
                $safe_MAKE = mysqli_real_escape_string( $link, $_REQUEST["make"] );
                $safe_MODEL = mysqli_real_escape_string( $link, $_REQUEST["model"] );
                $safe_MILEAGE = mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
                $safe_YEAR = mysqli_real_escape_string( $link, $_REQUEST["year"] );
                $safe_ID=mysqli_real_escape_string($link,$_REQUEST["ID"]);
                
               if(!preg_match("/^[A-Z]+$/",$Make){
                   print "you entered an invalid make"
               })
                if(!preg_match("/^[A-Z]+$/",$model){
                   print "you entered an invalid model"
               })
                if(!preg_match("/^[0-9]+$/",$mileage){
                   print "you entered an invalid mileage"
               }) 
               if(!preg_match("/^[0-9][0-9][0-9][0-9]$/",$year){
                   print "you entered an invalid year"
               })
                $query = "INSERT INTO cars ( MAKE, MODEL,MILEAGE,YEAR ) VALUES ( '$safe_MAKE', '$safe_MODEL', '$safe_MILEAGE', '$safe_YEAR')";
              

                $res= mysqli_query( $link, $query );
                 if(!$res){
                   print mysqli_error($link);
            }
            
            }
            
            
            if($_REQUEST["OPERATION"]=="DELETE"){
              $i = $_REQUEST['ID'];
             $DEL= "DELETE FROM cars WHERE ID = $i";
               mysqli_query( $link, $DEL);
            }
            
            
            
            $results = mysqli_query( $link, 'SELECT * FROM cars' );
             while( $record = mysqli_fetch_assoc( $results) ) {
                $ID = $record['ID'];
            	$Make = $record['MAKE'];
            	$model=$record['MODEL'];
            	$mileage = $record['MILEAGE'];
            	$year = $record['YEAR'];
            	$del=$record['ID'];
            
            	//"DELETE FROM 'cars' WHERE 'cars'.'ID'= ID"
            	print "<tr><td>$ID</td><td>$Make</td><td>$model</td><td>$mileage</td><td>$year</td><td><form class='needs-validation' method='POST'><input type='hidden' name='OPERATION' value='DELETE'><input type ='hidden'name='ID' value='$ID'><button class='btn btn-danger' type='submit'>Delete</button></input></form></td></tr>";
            }
               
            mysqli_free_result( $results );
            mysqli_close( $link );
            ?>

            
          

            </tbody>
        </table>
    
        <?php } ?>
                           

            <?php if($_Session["loggedin"]==false){
           //&&($_REQUEST["OPERATION"])=="ADD"
           
             $OPERATION=mysqli_real_escape_string($link,$_REQUEST["OPERATION"]);
             if ( isset( $_REQUEST["make"] )) {
                $safe_MAKE = mysqli_real_escape_string( $link, $_REQUEST["make"] );
                $safe_MODEL = mysqli_real_escape_string( $link, $_REQUEST["model"] );
                $safe_MILEAGE = mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
                $safe_YEAR = mysqli_real_escape_string( $link, $_REQUEST["year"] );
                $safe_ID=mysqli_real_escape_string($link,$_REQUEST["ID"]);
                if(!preg_match("/^[A-Z]+$/",$Make)){
                    print "make is incorrect";
       
                     }
                $query = "INSERT INTO cars ( MAKE, MODEL,MILEAGE,YEAR ) VALUES ( '$safe_MAKE', '$safe_MODEL', '$safe_MILEAGE', '$safe_YEAR')";
              

                mysqli_query( $link, $query );
            }
            
            
            if($_REQUEST["OPERATION"]=="DELETE"){
              $i = $_REQUEST['ID'];
             $DEL= "DELETE FROM cars WHERE ID = $i";
               mysqli_query( $link, $DEL);
            }
            
            
            
            $results = mysqli_query( $link, 'SELECT * FROM cars' );
             while( $record = mysqli_fetch_assoc( $results) ) {
                $ID = $record['ID'];
            	$Make = $record['MAKE'];
            	$model=$record['MODEL'];
            	$mileage = $record['MILEAGE'];
            	$year = $record['YEAR'];
            	$del=$record['ID'];
            
            	//"DELETE FROM 'cars' WHERE 'cars'.'ID'= ID"
            	print "<tr><td>$ID</td><td>$Make</td><td>$model</td><td>$mileage</td><td>$year</td></tr>";
            }
               
            mysqli_free_result( $results );
            mysqli_close( $link );
            
           
            
            }?>
           
        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
   
  </body>
</html>
