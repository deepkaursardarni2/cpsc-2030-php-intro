<?php 

 session_start();
 
    
    header( "Content-type: application/json");
     gettext();
    $link = mysqli_connect( 'localhost', 'root', 'smiley00' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    $dbName = "cars2";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $results = mysqli_query( $link, 'select * from cars' );
            
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $shoppinglist[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $shoppinglist );
            }
            break;
        
        case 'POST':
            $safe_Make = mysqli_real_escape_string( $link, $_REQUEST["MAKE"] );
            $safe_Model = mysqli_real_escape_string( $link, $_REQUEST["MODEL"] );
            //$ID=$_REQUEST("idREq");
            $Mileage = $_REQUEST["MILEAGE"] + 0;
            if ( strlen( $safe_Make ) <= 0 ||
                 strlen( $safe_Make) > 80 ||
                 $Mileage <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }
             $Year = $_REQUEST["YEAR"] + 0;
            if ( strlen( $safe_Model ) <= 0 ||
                 strlen( $safe_Model ) > 80 ||
                 $Year <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }

            $query = "INSERT INTO cars (MAKE,MODEL,MILEAGE,YEAR) VALUES ( '$safe_Make','$safe_Model','$Mileage ','$Year')";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;
        
        case 'DELETE':
            
            $ID = $_REQUEST["ID"];
           
             $DEL= "DELETE FROM cars WHERE ID = $ID";
             if ( ! mysqli_query( $link, $DEL ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            
            break;

    }
