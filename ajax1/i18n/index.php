<?php
    $locale = "fr_CA";
    
    $name = $_REQUEST["name"];
    $unread = $_REQUEST["unread"];

    putenv("LANG=" . $locale); 
    setlocale(LC_ALL, $locale);
    
    $domain = "example";
    bindtextdomain($domain, "Locale"); 
    bind_textdomain_codeset($domain, 'UTF-8');
    textdomain($domain);
?>
<!doctype html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>XXX</title>
  </head>
  <body class="container">
    <h1>XXX</h1>

    <p><?php echo gettext('We\'re now translating some strings') ?></p> 
    
    <p><?php echo sprintf( gettext('Hello %1$s! Your last visit was on %2$s' ), $name, strftime( "%A %B %e" ) ) ?></p> 

    <p><?php echo sprintf( ngettext('Only one unread message', '%d unread messages', $unread ), $unread ) ?></p> 

  </body>
</html>