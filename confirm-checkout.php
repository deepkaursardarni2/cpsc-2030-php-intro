



<?php
$firstName = trim( $_GET["firstName"]);
$lastName = trim( $_GET["lastName"]);
$email = trim( $_GET["email"]);
$address1 = trim( $_GET["address1"]);
$address2 = trim( $_GET["address2"]);
$country = trim( $_GET["country"]);
$province = trim( $_GET["province"]);
$postalCode = trim( $_GET["postalCode"]);
$deliveryMethod=trim( $_GET["deliveryMethod"]);
$giftWrapping = trim( $_GET["giftWrapping"]);
$ccName = trim( $_GET["ccName"]);
$ccNumber = trim( $_GET["ccNumber"]);
$ccExpiration = trim( $_GET["ccExpiration"]);
$ccCvv= trim( $_GET["ccCvv"]);
$quantity1 = trim( $_GET["quantity1"]);
$quantity2 = trim( $_GET["quantity2"]);
$quantity3 = trim( $_GET["quantity3"]);
?>





<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Checkout example · Bootstrap</title>

    

    <!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
   <div class="container">
  
   <div class="row">
        <div class="col-md-5 order-md-1">
              <h2 class="d-flex justify-content-between align-items-center mb-3">order Confirmation</h2>
               <div class="col-md-11 order-md-3 mb-4">
                 
                      First name: <?php echo $firstName ?><br>
                      Last name: <?php print $lastName ?><br>
                     email: <?php print $email ?><br>
                    address: <?php print $address1 ?><br>
                     address 2:<?php print $address2?><br>
                     Country: <?php print $country ?><br>
                      Province: <?php print $province ?><br>
                      Postal code: <?php print $postalCode ?>
                    
               </div>
           </div>
   
        

              <div class="col-md-5 order-md-1">
                <h3 class="d-flex justify-content-between align-items-center mb-3">credit card info</h3>
                    <div class="col-md-11 order-md-3 mb-4">
                              Name on card:<?php print $ccName ?><br>
                        credit card number:<?php print $ccNumber ?><br>
                        credit card expiry:<?php print $ccExpiration ?><br>
                        credit card cvv:<?php print $ccCvv ?><br>
                   </div>
             
               </div>
         


                 <div class="col-md-5 order-md-1">
                      <h3 class="d-flex justify-content-between align-items-center mb-3">order subtotal</h3>
                          <div class="col-md-11 order-md-3 mb-4">
                  
                               quantity1=<?php print $quantity1 ?><br>
                               quantity2=<?php print $quantity2 ?><br>
                               quantity3=<?php print $quantity3 ?><br>
                              <?php $subtotal=($quantity1*12)+($quantity2*8)+($quantity3*5);?>
                              <?php $grandTotal=0?>
                               <?php $taxTotal=0?>
                                delievery method choosen : <?php print $deliveryMethod ?><br>
                                      gift Wrapping :<?php  if($giftWrapping==on)
                                     print "yes"?>
                              <br>
                            subtotal= $ <?php print $subtotal ?>
                          </div>
                  
                 </div>
            
            

   
                 <div class="col-md-5 order-md-1">
                
                    <h4 class="d-flex justify-content-between align-items-center mb-3">Order total and taxes</h4>
                      <div class="col-md-11 order-md-3 mb-4"><?php 
                             switch($province){
                                 case (Ontario):
                                    print "hst:".$subtotal*0.08;
                                    print "<br> gst:".$subtotal*.05;
                                    $taxTotal=.13*$subtotal;
                                    print "<br>total-taxes:$"..13*$subtotal;
                                     break;
                                 case (British_columbia):
                                 print "pst:".$subtotal*.07;
                                    print  "<br> gst:".$subtotal*.05;
                                    $taxTotal=.12*$subtotal;
                                    print "<br>total-taxes:$"..12*$subtotal;
                                 break;
                                 
                                 case (Alberta):
                                 print "hst/pst:".$subtotal*.0;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.05*$subtotal;
                                    print "<br>total-taxes:$"..05*$subtotal;
                                 break;
                                 
                                 case (Quebec):
                                 print "qst:".$subtotal*.09975;
                                    print "<br>gst:" .$subtotal*.05;
                                    $taxTotal=.14975*$subtotal;
                                    print "<br>total-taxes:$"..14975*$subtotal;
                                 break;
                                 
                                 case (Nova_scotia):
                                 print "hst:".$subtotal*.10;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.15*$subtotal;
                                    print "<br>total-taxes:$"..15*$subtotal;
                                 break;
                                 
                                 case (saskatchewan):
                                 print "hst:".$subtotal*.06;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.11*$subtotal;
                                    print "<br>total-taxes:$"..11*$subtotal;
                                 break;
                                 
                                 case (manitoba):
                                 print "hst:".$subtotal*.08;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.13*$subtotal;
                                    print "<br>total-taxes:$"..13*$subtotal;
                                 break;
                                 
                                 case (New Brunswick):
                                 print "hst:".$subtotal*.10;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.15*$subtotal;
                                    print "<br>total-taxes:.$".$subtotal*.15;
                                 break;
                                 
                                 case (Newfoundland and Labrador):
                                 print "hst:".$subtotal*.10;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.15*$subtotal;
                                    print "<br>total-taxes:$"..15*$subtotal;
                                  break;
                                 
                                 case (Prince_Edward_island):
                                 print "hst:".$subtotal*.10;
                                    print "<br>gst:".$subtotal*.05;
                                    $taxTotal=.15*$subtotal;
                                    print "<br>total-taxes:$"..15*$subtotal;
                                 break;
                                 
                                 default:
                                     $taxTotal=.05*$subtotal;
                                     print "gst:$".$subtotal*.05;
                             }
                                 ?>
                
                
                    </div>
            </div>
               
            <div>
               <?php  
               $grandTotal=$subtotal+$taxTotal;
               
                if($giftWrapping==on){
                 $grandTotal=$grandTotal+4;
                }
                
                
                
                if($deliveryMethod==$mail){
                   $grandTotal=$grandTotal+6;
                }
                
                 else if($deliveryMethod==$courier){
                   $grandTotal=$grandTotal+8;
                }
                 else{
                   $grandTotal=$grandTotal+3;
                }
                
                  ?>
            </div>
              
              
            <div class="col-md-5 order-md-1">
               
                   <h4 class="d-flex justify-content-between align-items-center mb-3">Grand total(including shipping and wrapping charges if choosen)</h4>
                      <div class="col-md-11 order-md-3 mb-4">
                          <?php
                                print "grand total:$".$grandTotal;
                                   if($grandTotal>750){
                                    print "<br><strong>payment declined order too large!</strong>";
                                   }else{
                                       print "<br><strong>Order confirmed</strong>";
                                   }
                         
                           ?>
                       </div>
             </div>

</div>
</div>